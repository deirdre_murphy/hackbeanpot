import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import Swipe from '../pages/swipe';
import '../layouts/index.css';
import FilteredDogs from '../pages/current-dogs';

var lastButtonGroupStyle = {
  margin: '0px 0px 50px 0px',
};
export function sendFormData() {
  var activity = document.querySelector('input[name="activity"]:checked').value;
  var age = document.querySelector('input[name="age"]:checked').value;
  var size = document.querySelector('input[name="size"]:checked').value;
  var kids = document.querySelector('input[name="kids"]:checked').value;
  var dogs = document.querySelector('input[name="friendliness"]:checked').value;
  var sex = document.querySelector('input[name="sex"]:checked').value;
  var resultDogs = FilteredDogs(activity, age, size, kids, dogs, sex);
  return resultDogs;
}

export default function Form() {
  return (
    <div className="wrapper">
      <form onSubmit="sendFormData();">
        <h1 style={{ color: '#03045e' }}>Let's learn a bit about you!</h1>
        <specs>Name</specs>
        <input name="name" />
        <label>
          <specs>Email</specs>
          <input email="email" />
        </label>
        <h1>What type of dog are you looking for?</h1>
        <div className="form-check">
          <label>
            <specs>Age of Dog</specs>
            <input type="radio" name="age" value="puppy" className="form-check-input" defaultChecked />
            Puppy
            <input type="radio" name="age" value="young" className="form-check-input" />
            Young
            <input type="radio" name="age" value="adult" className="form-check-input" />
            Adult
            <input type="radio" name="age" value="senior" className="form-check-input" />
            Senior
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs>Sex</specs>
            <input type="radio" name="sex" value="male" className="form-check-input" defaultChecked />
            Male
            <input type="radio" name="sex" value="female" className="form-check-input" />
            Female
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs>Dog Size</specs>
            <input type="radio" name="size" value="small" className="form-check-input" defaultChecked />
            Small
            <input type="radio" name="size" value="medium" className="form-check-input" />
            Medium
            <input type="radio" name="size" value="large" className="form-check-input" />
            Large
            <input type="radio" name="size" value="huge" className="form-check-input" />
            Huge
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs>Activity Level</specs>
            <input type="radio" name="activity" value="low" className="form-check-input" defaultChecked />
            Chill
            <input type="radio" name="activity" value="medium" className="form-check-input" />
            Medium
            <input type="radio" name="activity" value="high" className="form-check-input" />
            Active
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs>Sociable with other animals?</specs>
            <input type="radio" name="friendliness" value="yes" className="form-check-input" defaultChecked />
            Yes
            <input type="radio" name="friendliness" value="no" className="form-check-input" />
            No
          </label>
        </div>
        <div className="form-check" style={lastButtonGroupStyle}>
          <label>
            <specs>Good with kids?</specs>
            <input type="radio" name="kids" value="yes" className="form-check-input" defaultChecked />
            Yes
            <input type="radio" name="kids" value="no" className="form-check-input" />
            No
          </label>
        </div>
        <Link to="/pages/swipe">Find Dogs!</Link>
      </form>
    </div>
  );
}
