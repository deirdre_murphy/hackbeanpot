import React, { navigation, useState } from 'react';
import Swipe from '../pages/swipe';
import '../layouts/index.css';
import { Link } from 'react-router-dom';

var formStyle = {
  position: 'absolute',
  width: '20vw',
  height: '100vh',
  border: '6px solid #03045e',
  backgroundColor: '#90e0ef',
  top: '0%',
  left: '0%',
};

var linkStyle = {
  padding: '10px',
};

var headerTextStyle = {
  margin: '0px 0px 10px 0px',
  fontSize: '25px',
};

var buttonTextStyle = {
  fontSize: '25px',
  marginBotton: '10px',
};

export default function Sideform() {
  return (
    <div className="wrapper">
      <form style={formStyle}>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Age of Dog</specs>
            <input
              type="radio"
              name="age"
              value="puppy"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Puppy
            <input type="radio" name="age" value="young" className="form-check-input" style={buttonTextStyle} />
            Young
            <input type="radio" name="age" value="adult" className="form-check-input" style={buttonTextStyle} />
            Adult
            <input type="radio" name="age" value="senior" className="form-check-input" style={buttonTextStyle} />
            Senior
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Sex</specs>
            <input
              type="radio"
              name="sex"
              value="male"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Male
            <input type="radio" name="sex" value="female" className="form-check-input" style={buttonTextStyle} />
            Female
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Dog Size</specs>
            <input
              type="radio"
              name="size"
              value="small"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Small
            <input type="radio" name="size" value="medium" className="form-check-input" style={buttonTextStyle} />
            Medium
            <input type="radio" name="size" value="large" className="form-check-input" style={buttonTextStyle} />
            Large
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Activity Level</specs>
            <input
              type="radio"
              name="activity"
              value="chill"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Low
            <input type="radio" name="activity" value="medium" className="form-check-input" style={buttonTextStyle} />
            Medium
            <input type="radio" name="activity" value="active" className="form-check-input" style={buttonTextStyle} />
            High
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Sociable with other animals?</specs>
            <input
              type="radio"
              name="friendliness"
              value="yes"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Yes
            <input type="radio" name="friendliness" value="no" className="form-check-input" style={buttonTextStyle} />
            No
          </label>
        </div>
        <div className="form-check">
          <label>
            <specs style={headerTextStyle}>Good with kids?</specs>
            <input
              type="radio"
              name="kids"
              value="yes"
              className="form-check-input"
              style={buttonTextStyle}
              defaultChecked
            />
            Yes
            <input type="radio" name="kids" value="no" className="form-check-input" style={buttonTextStyle} />
            No
          </label>
        </div>
        <Link to="/pages/swipe" style={linkStyle}>
          RESET
        </Link>
      </form>
    </div>
  );
}
