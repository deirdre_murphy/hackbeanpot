import React from 'react';
import { Link } from 'react-router-dom';
import dogoBG1 from '../dog-data/DogoBG1.jpg';

var backgroundStyle = {
  display: 'flex',
  backgroundColor: '#00b4d8',
  width: '100vw',
  height: '100vh',
  flexDirection: 'column',
  alignItems: 'center',
};

var headerStyle = {
  fontSize: '85px',
  fontFamily: 'Monaco',
  textAlign: 'center',
};

var textStyle = {
  fontSize: '25px',
  fontFamily: 'Monaco',
  textAlign: 'center',
  margin: '30px 60px 30px 60px',
};

var linkStyle = {
  fontSize: '25px',
  fontFamily: 'Monaco',
  textAlign: 'center',
  margin: '30px 60px 30px 60px',
};

var backgroundPicStyle = {
  height: '300px',
  width: '700px',
};

export default function Homepage() {
  return (
    <div style={backgroundStyle}>
      <div style={headerStyle}>WELCOME TO DOGGO!</div>
      <div style={textStyle}>
        We know how stressful the dog adoption process is: trying to find an adoption centre close to you, looking for a
        dog, filling out the form, verification, interviews. At Doggo, we make it as seamless and stress-free as
        possible!!!
      </div>
      <Link to="/pages/form" style={linkStyle}>
        Create Profile
      </Link>
      <img src={dogoBG1} style={backgroundPicStyle}></img>
    </div>
  );
}
