//filtered dogs to present in swipe
import { dogs } from '../dog-data/dogs.json';

export default function FilteredDogs({ activity, age, size, kids, dogs, sex }) {
  //must haves
  const dogsBySex = dogs.filter((d) => d.sex == sex);
  const dogsBySize = dogs.filter((d) => d.size == size);

  var randOther = Math.floor(Math.random() * 4);
  const dogsByActivity = dogs.filter((d) => d.activity == activity);
  const dogsByAge = dogs.filter((d) => d.age == age);
  const dogsByKids = dogs.filter((d) => d.kids == kids);
  const dogsByDogs = dogs.filter((d) => d.dogs == dogs);
  var otherCats = [dogsByActivity, dogsByAge, dogsByKids, dogsByDogs];

  var allDogs = dogsBySex.concat(dogsBySize, otherCats[randOther]);
  var uniqueDogs = [...new Set(allDogs)];
  return uniqueDogs;
}
