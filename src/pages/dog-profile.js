import React from 'react';
import arrow1 from '../images/arrow1.svg';
import arrow2 from '../images/arrow2.svg';

export default function ProfileBox({ name, description, photo }) {
  return (
    <div style={profileStyle}>
      <img src={photo} style={imageStyle}></img>
      <div style={nameStyle}> {name} </div>
      <div style={profileTextStyle}> {description} </div>
      <button style={buttonStyle1}>Wag Left</button>
      <button style={buttonStyle2}>Wag Right</button>
    </div>
  );
}

var buttonStyle1 = {
  position: 'absolute',
  top: '73%',
  left: '-10%',
  height: '45vh',
  width: '20vw',
  backgroundImage: 'url(' + arrow1 + ')',
  backgroundColor: 'transparent',
  backgroundSize: '100% 100%',
  padding: '0',
  border: 'none',
};

var buttonStyle2 = {
  position: 'absolute',
  top: '73%',
  right: '-10%',
  height: '45vh',
  width: '20vw',
  backgroundImage: 'url(' + arrow2 + ')',
  backgroundColor: 'transparent',
  backgroundSize: '100% 100%',
  padding: '0',
  border: 'none',
};

var nameStyle = {
  position: 'absolute',
  width: '203px',
  height: '25px',
  fontFamily: 'Droid Sans',
  fontWeight: 'bold',
  fontSize: '35px',
  textAlign: 'center',
  color: '#000000',
  top: '74%',
  bottom: '50%',
  left: '33%',
  right: '50%',
  position: 'absolute',
};

var profileTextStyle = {
  position: 'absolute',
  width: '350px',
  height: '77px',
  fontFamily: 'Droid Sans',
  fontWeight: 'normal',
  fontSize: '16px',
  textAlign: 'center',
  color: '#000000',
  top: '82%',
  left: '22%',
  position: 'absolute',
};

var profileStyle = {
  position: 'absolute',
  width: '600px',
  height: '600px',
  top: '2%',
  bottom: '50%',
  left: '30%',
  right: '50%',
  background: '#00b4d8',
  border: '15px',
  borderRadius: '20%',
};

var imageStyle = {
  width: '400px',
  height: '400px',
  position: 'absolute',
  top: '5%',
  bottom: '50%',
  left: '17%',
  right: '50%',
  border: '3px solid #FFFFFF',
  borderRadius: '20%',
};
