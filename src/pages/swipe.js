import React from 'react';
import esmeralda from '../dog-data/esmeralda.png';
import Sideform from './sideform.js';
import Scrollpanel from './scrollpanel.js';
import ProfileBox from '../pages/dog-profile';
import sendFormData from '../pages/form';
import { dogs } from '../dog-data/dogs.json';

var swipePageStyle = {
  display: 'flex',
  flexDirection: 'row',
  alignContent: 'center',
  justifyContent: 'space-between',
  backgroundColor: '#caf0f8',
};

var dogsToShow = sendFormData().values;

export default function Swipe() {
  return (
    <div style={swipePageStyle}>
      <Sideform></Sideform>
      <ProfileBox name={'Esmeralda'} photo={esmeralda} description={dogs.Esmeralda.desc}></ProfileBox>
      <Scrollpanel></Scrollpanel>
    </div>
  );
}
