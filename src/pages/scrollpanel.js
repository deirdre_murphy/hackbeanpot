import React from 'react';
import yarn from '../dog-data/yarn.png';
import zeus from '../dog-data/zeus.png';
import wonton from '../dog-data/wonton.png';
import xander from '../dog-data/xander.png';
import vermont from '../dog-data/vermont.png';
import tuba from '../dog-data/tuba.png';

var scrollStyle = {
  position: 'absolute',
  top: '0%',
  right: '0%',
  height: '100vh',
  width: '20vw',
  background: '#90e0ef',
  overflow: 'auto',
  overflowY: 'scroll',
  display: 'flex',
  flexDirection: 'column',
  alignContent: 'space-between',
  alignItems: 'center',
  border: '6px solid #03045e',
};

var imageStyle = {
  width: '200px',
  height: '200px',
  background: '#00b4d8',
  padding: '5px 5px 5px 5px',
  margin: '10px 10px 5px 10px',
  borderRadius: '20%',
  padding: '7px',
};

var nameStyle = {
  fontFamily: 'Droid Sans',
  fontWeight: 'bold',
  fontSize: '25px',
  textAlign: 'center',
  margin: '5px 5px 5px 5px',
  color: '#000000',
};

var sideImgStyle = {
  border: '3px solid #FFFFFF',
  borderRadius: '20%',
  background: '#00b4d8',
  margin: '10px 10px 10px 10px',
};

export default function Scrollpanel() {
  return (
    <div style={scrollStyle}>
      <div style={sideImgStyle}>
        <img src={yarn} style={imageStyle}></img>
        <div style={nameStyle}>Yarn</div>
      </div>
      <div style={sideImgStyle}>
        <img src={wonton} style={imageStyle}></img>
        <div style={nameStyle}>Wonton</div>
      </div>
      <div style={sideImgStyle}>
        <img src={xander} style={imageStyle}></img>
        <div style={nameStyle}>Xander</div>
      </div>
      <div style={sideImgStyle}>
        <img src={vermont} style={imageStyle}></img>
        <div style={nameStyle}>Vermont</div>
      </div>
      <div style={sideImgStyle}>
        <img src={tuba} style={imageStyle}></img>
        <div style={nameStyle}>Tuba</div>
      </div>
    </div>
  );
}
