import React from 'react';
import Form from '../pages/form';
import Swipe from '../pages/swipe';
import Homepage from '../pages/homepage';
import { Switch, Route } from 'react-router-dom';

export default function Home() {
  return (
    <div>
      <Switch>
        <Route exact path="/">
          <Homepage />
        </Route>
        <Route exact path="/pages/form">
          <Form />
        </Route>
        <Route path="/pages/swipe">
          <Swipe />
        </Route>
      </Switch>
    </div>
  );
}
