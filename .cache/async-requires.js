// prefer default export if available
const preferDefault = m => m && m.default || m

exports.components = {
  "component---cache-dev-404-page-js": require("gatsby-module-loader?name=component---cache-dev-404-page-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/dev-404-page.js"),
  "component---src-pages-404-js": require("gatsby-module-loader?name=component---src-pages-404-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/404.js"),
  "component---src-pages-current-dogs-js": require("gatsby-module-loader?name=component---src-pages-current-dogs-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/current-dogs.js"),
  "component---src-pages-dog-profile-js": require("gatsby-module-loader?name=component---src-pages-dog-profile-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/dog-profile.js"),
  "component---src-pages-form-js": require("gatsby-module-loader?name=component---src-pages-form-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/form.js"),
  "component---src-pages-homepage-js": require("gatsby-module-loader?name=component---src-pages-homepage-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/homepage.js"),
  "component---src-pages-scrollpanel-js": require("gatsby-module-loader?name=component---src-pages-scrollpanel-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/scrollpanel.js"),
  "component---src-pages-sideform-js": require("gatsby-module-loader?name=component---src-pages-sideform-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/sideform.js"),
  "component---src-pages-swipe-js": require("gatsby-module-loader?name=component---src-pages-swipe-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/src/pages/swipe.js")
}

exports.json = {
  "layout-index.json": require("gatsby-module-loader?name=path---!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/layout-index.json"),
  "dev-404-page.json": require("gatsby-module-loader?name=path---dev-404-page!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/dev-404-page.json"),
  "404.json": require("gatsby-module-loader?name=path---404!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/404.json"),
  "current-dogs.json": require("gatsby-module-loader?name=path---current-dogs!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/current-dogs.json"),
  "dog-profile.json": require("gatsby-module-loader?name=path---dog-profile!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/dog-profile.json"),
  "form.json": require("gatsby-module-loader?name=path---form!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/form.json"),
  "homepage.json": require("gatsby-module-loader?name=path---homepage!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/homepage.json"),
  "scrollpanel.json": require("gatsby-module-loader?name=path---scrollpanel!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/scrollpanel.json"),
  "sideform.json": require("gatsby-module-loader?name=path---sideform!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/sideform.json"),
  "swipe.json": require("gatsby-module-loader?name=path---swipe!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/swipe.json"),
  "404-html.json": require("gatsby-module-loader?name=path---404-html!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/json/404-html.json")
}

exports.layouts = {
  "layout---index": require("gatsby-module-loader?name=component---src-layouts-index-js!/Users/deirdremurphy/Documents/dogtinder/hackbeanpot/.cache/layouts/index.js")
}